package com.example.android.oscebosskey;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class SpecificStationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_list_two);

        String specificStationsList = getIntent().getStringExtra("specificStationsList");


        final ArrayList<String> records = new ArrayList<String>();

        if (specificStationsList.equals("Chest Pain")) {
            records.add(new String("Wash Hands"));
            records.add(new String("Introduce Yourself"));
            records.add(new String("Establish Rapport"));
        } else if (specificStationsList.equals("Abdominal Pain")) {
            records.add(new String("Wash Hands"));
            records.add(new String("Abdo pain Yourself"));
            records.add(new String("Further Abdopain Question time"));
        } else if (specificStationsList.equals("Headache")) {
            records.add(new String("Wash Hands"));
            records.add(new String("Headache Question"));
            records.add(new String("Further Headache Question"));
        } else if (specificStationsList.equals("Shortness of Breath")){
            records.add(new String("Appropriate introduction"));
            records.add(new String("Explains purpose of interview and checks consent"));
            records.add(new String("Starts with an open question and listens without interrupting"));
            records.add(new String("Establishes the onset of shortness of breath"));
            records.add(new String("Establishes the duration of shortness of breath"));
            records.add(new String("Enquires after the triggers"));
            records.add(new String("Asks about the progression"));
            records.add(new String("Enquires after severity of SoB"));
            records.add(new String("Asks about which factors relieve the shortness of breath"));
            records.add(new String("Detailed history about associated cardio-respiratory symptoms" +
                    "such as chest pain, palpitations, orthopnoea, PND, haemoptysis, sputum"));
            records.add(new String("Asks about associated systemic symptoms such as nausea"));
            records.add(new String("Asks about risk factors for VTE"));
            records.add(new String("Asks about risk factors for heart disease"));
            records.add(new String("Enquires about previous similar episodes"));
            records.add(new String("Completes ICE"));
            records.add(new String("Invites Questions"));
            records.add(new String("Organised Approach"));
        } else
            records.add(new String("There are no individual entries written for this station"));


        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, R.layout.end_list_item, records);
        final ListView listView = (ListView) findViewById(R.id.record_list_two);
        listView.setAdapter(itemsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listView.getChildAt(position).setBackgroundColor(ContextCompat.getColor(SpecificStationActivity.this, R.color.colorGreen));
            }
            });
    }
}