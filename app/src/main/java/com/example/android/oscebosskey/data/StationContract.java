package com.example.android.oscebosskey.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by peterwoodward-court on 15/10/2017.
 */

public final class StationContract {

    private StationContract(){}

    public static final String CONTENT_AUTHORITY = "com.example.android.oscebosskey";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_STATIONS = "stations";


    public static final class StationEntry implements BaseColumns{

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_STATIONS);

        /**
         * The MIME type of the {@link #CONTENT_URI} for a list of stations.
         */
        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_STATIONS;

        /**
         * The MIME type of the {@link #CONTENT_URI} for a single station.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_STATIONS;

        public final static String TABLE_NAME = "station_list";
        public final static String _ID = BaseColumns._ID;
        public final static String STATIONNAME = "station";
    }
}
