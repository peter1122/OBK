package com.example.android.oscebosskey.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.android.oscebosskey.data.StationContract.StationEntry;

public class StationsDbHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = StationsDbHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "stations.db";

    private static final int DATABASE_VERSION = 1;

    public StationsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_STATIONLIST = "CREATE TABLE " + StationEntry.TABLE_NAME + " ("
        + StationEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + StationEntry.STATIONNAME + " TEXT);";
        Log.v(LOG_TAG,SQL_CREATE_STATIONLIST);
        db.execSQL(SQL_CREATE_STATIONLIST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


}

