package com.example.android.oscebosskey;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.database.Cursor;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android.oscebosskey.data.StationContract.StationEntry;


public class NewSpecificStation extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int STATION_LOADER = 0;

    StationCursorAdapter mCursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_list_two);

        // Setup FAB to open AddStationActivity
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewSpecificStation.this, AddStationActivity.class);
                startActivity(intent);
            }
        });

        // Find the ListView which will be populated with the pet data
        ListView stationListView = (ListView) findViewById(R.id.record_list_two);

        // Find and set empty view on the ListView, so that it only shows when the list has 0 items.
        View emptyView = findViewById(R.id.empty_view);
        stationListView.setEmptyView(emptyView);

        mCursorAdapter = new StationCursorAdapter(this, null);
        stationListView.setAdapter(mCursorAdapter);

        getLoaderManager().initLoader(STATION_LOADER, null, this);

    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                StationEntry._ID,
                StationEntry.STATIONNAME };

        return new CursorLoader(this,
                StationEntry.CONTENT_URI,
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }



}

